import java.util.Scanner;

public class NationalPark
{
	public static void main(String[] args)
	{
		Penguin[] waddle = new Penguin[4];
		Scanner scan = new Scanner(System.in);
		
		for(int i = 0; i < waddle.length; i++)
		{
			
			System.out.println("Enter the penguin's name");
			String name = scan.next();
			System.out.println("Enter the penguin's type");
			String type = scan.next();
			System.out.println("Enter the penguin's age");
			int age = scan.nextInt();
			System.out.println("Enter the penguin's gender(Male/Female)");
			String gender = scan.next();
			waddle[i] = new Penguin(type, name, age, gender);
		}
		
		waddle[0].penguinStats();
		System.out.println(" ");
		waddle[1].penguinStats();
		System.out.println(" ");
		waddle[2].penguinStats();
		System.out.println(" ");
		waddle[3].penguinStats();
		System.out.println(" ");
		
		waddle[0].hunt();
		System.out.println(" ");
		waddle[0].lead();
		System.out.println(" ");
		waddle[0].lead();
		System.out.println(" ");
		waddle[0].caughtFish();
		System.out.println(" ");
		
		System.out.println("How many years shall be added to " + waddle[1].getName() + " who is " + waddle[1].getAge() + " years old?");
		int addedYears = scan.nextInt();
		waddle[1].increaseAge(addedYears);
		System.out.println(waddle[1].getName() + " is now " + waddle[1].getAge() + " years old.");
		
		System.out.println(" ");
		waddle[3].penguinStats();
		System.out.println("Enter the 4th penguin's new name");
		String name = scan.next();
		waddle[3].setName(name);
		System.out.println("Enter the 4th penguin's new age");
		int age = scan.nextInt();
		waddle[3].setAge(age);
		System.out.println(" ");
		waddle[3].penguinStats();
		
		
	}
}
		