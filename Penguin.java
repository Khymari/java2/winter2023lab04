import java.util.Random;

public class Penguin
{
	private String type;
	private String name;
	private int age;
	private char gender;
	
	int fishCaught;
	boolean isLeading;
	
	//Constructor
	public Penguin(String nType, String nName, int nAge, String nGender)
	{
		if(isTypeValid(nType))
			this.type = nType;
		if(nName.length() >= 3)
			this.name = nName;
		if(nAge > 0 && nAge < 21)
			this.age = nAge;
		
		char gend = nGender.charAt(0);
		gend = genderCapitalization(gend);
		if(isGenderValid(gend))
			this.gender = gend;
	}
	
	//Setters
	// public void setType(String newType)
	// {
		// if(isTypeValid(newType))
			// this.type = newType;
	// }
	
	public void setName(String newName)
	{
		if(newName.length() >= 3)
			this.name = newName;
	}
	
	public void setAge(int newAge)
	{
		if(newAge > 0 && newAge < 21)
			this.age = newAge;
	}
	
	public void setGender(String newGender)
	{
		char gend = newGender.charAt(0);
		gend = genderCapitalization(gend);
		
		if(isGenderValid(gend))
			this.gender = gend;
	}
	
	//Getters
	public String getType()
	{
		return this.type;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public int getAge()
	{
		return this.age;
	}
	
	public char getGender()
	{
		return this.gender;
	}
	
	//normal Methods
	public void penguinStats()
	{
		System.out.println("Name: " + this.name + "\nType: " + this.type + "\nAge: " + this.age + "\nSex: " + this.gender);
	}
	
	public void hunt()
	{
		
		if(this.age < 2)
		{
			System.out.println(this.name + " can not hunt for they are too young");
		}
		else
		{
			for(int i = 0; i < 10; i++)
			{
				Random rand = new Random();
				int randNum = rand.nextInt(5);
				if(randNum == 4)
				{
					System.out.println(this.name + " could not catch a fish.");
				}
				else
				{
					System.out.println(this.name + " caught a fish!");
					fishCaught++;
				}
			}
		}
	}
	
	public void lead()
	{
		if(this.age >= 4 && this.type.equals("Emperor"))
		{
			if(!this.isLeading)
			{
				System.out.println(this.name + " is now leading.");
				this.isLeading = true;
			}
			else
			{
				System.out.println(this.name + " is no longer leading.");
				this.isLeading = false;
			}
		}
		else
		{
			System.out.println(this.name + " can not lead.");
		}
	}
	
	public void caughtFish()
	{
		System.out.println(this.name + " caught: " + this.fishCaught + " fish.");
	}
	
	public void increaseAge(int addedYears)
	{
		if(isAgeValid(addedYears))
			this.age += addedYears;
	}
	
	//validation Methods
	private boolean isAgeValid(int years)
	{
		if(years > 0 && (years + this.age) < 21)
			return true;
		else 
			return false;
	}
	
	private boolean isTypeValid(String newType)
	{
		if(newType.equals("Emperor") || newType.equals("Little") || newType.equals("King") || newType.equals("African") || newType.equals("Macaroni") || newType.equals("Gentoo") || newType.equals("Humboldt") || newType.equals("Chinstrap") || newType.equals("Mangellanic"))
			return true;
		else
			return false;
	}
	
	private char genderCapitalization(char gen)
	{
		if(gen == 'm' || gen == 'f')
			gen = (char)(gen - 32);
			
		return gen;
	}
	
	private boolean isGenderValid(char gen)
	{		
		if(gen == 'M' || gen == 'F')
			return true;
		else
			return false;
	}
}